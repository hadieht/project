﻿
using Interfaces.IBalanceRepository;
using System.Data.SqlClient;
using Data.DataUser;
using Data.DataBalance;
using System.Collections.Generic;

namespace Repositories.BalanceRepository
{
    public class BalanceRepository : IBalanceRepository
    {
        private string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\ОСН прог\2\Project\DataBaces\DataBaces\MainDatabase.mdf;Integrated Security=True";

        public void СhangeBalanceMain()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {

                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand("UPDATE [Balances] SET main = @main  where Id = @Id ", sqlConnection);
                sqlCommand.Parameters.AddWithValue("Id", DataUser.Id);
                sqlCommand.Parameters.AddWithValue("main", DataBalance.Main);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }

        }

        public void СhangeBalanceStash()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {

                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand("UPDATE [Balances] SET stash = @stash  where Id = @Id ", sqlConnection);
                sqlCommand.Parameters.AddWithValue("Id", DataUser.Id);
                sqlCommand.Parameters.AddWithValue("stash", DataBalance.Stash);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }

        }


        public void СhangeBalanceAccumulation()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {

                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand("UPDATE [Balances] SET accumulation = @accumulation  where Id = @Id ", sqlConnection);
                sqlCommand.Parameters.AddWithValue("Id", DataUser.Id);
                sqlCommand.Parameters.AddWithValue("accumulation", DataBalance.Accumulation);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }

        }

        public int SumBalance()
        {
            int sum = 0;
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand("SELECT main FROM [Balances]", sqlConnection);
                sqlCommand.ExecuteNonQuery();

                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int Index_main = reader.GetOrdinal("main");

                        DataBalance.Main = reader.GetInt32(Index_main);

                        sum = sum + DataBalance.Main;
                    }
                }

                reader.Close();
                sqlConnection.Close();
                return sum;
            }
        }

        public void LoginBalance()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {

                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand("SELECT * from [Balances] where Id = @Id", sqlConnection);
                sqlCommand.Parameters.AddWithValue("Id", DataUser.Id);
                sqlCommand.ExecuteNonQuery();

                SqlDataReader reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                   
                    int Index_main = reader.GetOrdinal("main");
                    int Index_stash = reader.GetOrdinal("stash");
                    int Index_accumulation = reader.GetOrdinal("accumulation");

                    DataBalance.Main = reader.GetInt32(Index_main);
                    DataBalance.Stash = reader.GetInt32(Index_stash);
                    DataBalance.Accumulation = reader.GetInt32(Index_accumulation);
                   
                }
                reader.Close();
            }
        }

        public void RegBalance()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand("INSERT INTO [Balances] (main , stash , accumulation) VALUES (0,0,0) ", sqlConnection);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }

        }


        public void RemoveBalanceMain()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {

                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand("UPDATE [Balances] SET main = 0  where Id = @Id ", sqlConnection);
                sqlCommand.Parameters.AddWithValue("Id", DataUser.Id);
                DataBalance.Main = 0;
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }

        }

        public void RemoveBalanceAccumulation()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {

                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand("UPDATE [Balances] SET accumulation = 0  where Id = @Id ", sqlConnection);
                sqlCommand.Parameters.AddWithValue("Id", DataUser.Id);
                DataBalance.Accumulation = 0;
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }

        }

      public  void RemoveBalanceStash()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {

                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand("UPDATE [Balances] SET stash = 0  where Id = @Id ", sqlConnection);
                sqlCommand.Parameters.AddWithValue("Id", DataUser.Id);
                DataBalance.Stash = 0;
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }
        }

        public List<string[]> ShowBalanceUsers()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommandUser = new SqlCommand("Select [Users].id,login,name,main  from [Users]  LEFT JOIN [Balances] ON [Users].id =[Balances].id ", sqlConnection);
                
                SqlDataReader readerUser = sqlCommandUser.ExecuteReader();

                List<string[]> data = new List<string[]>();

                while (readerUser.Read())
                {
                    data.Add(new string[4]);
                    data[data.Count - 1][0] = readerUser[0].ToString();
                    data[data.Count - 1][1] = readerUser[1].ToString();
                    data[data.Count - 1][2] = readerUser[2].ToString();
                    data[data.Count - 1][3] = readerUser[3].ToString();
                }

                readerUser.Close();
                sqlConnection.Close();

                return data;
            }   
        }


    }
}
