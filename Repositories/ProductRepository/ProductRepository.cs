﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Interfaces.IProductRepository;
using Models.ProductModel;
using Data.DataProduct;
using Data.DataUser;

namespace Repositories.ProductRepository
{
    public class ProductRepository : IProductRepository
    {
        private string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\ОСН прог\2\Project\DataBaces\DataBaces\MainDatabase.mdf;Integrated Security=True";

        public void AddProduct(Product product)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand("INSERT INTO [Products] (name, price,state ,user_id ) VALUES ( @name , @price , @state , @user_id)", sqlConnection);
                sqlCommand.Parameters.AddWithValue("name", product.Name);
                sqlCommand.Parameters.AddWithValue("price", product.Price);
                sqlCommand.Parameters.AddWithValue("state", product.State);
                sqlCommand.Parameters.AddWithValue("user_id", product.IdUser);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }

        }


        public List<string[]> ShowBalanceProduct()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand("Select  [Products].name , price , state , [Users].name from [Products]  LEFT JOIN [Users] ON [Users].id = user_id ", sqlConnection);
                sqlCommand.ExecuteNonQuery();
                SqlDataReader reader = sqlCommand.ExecuteReader();

                List<string[]> data = new List<string[]>();

                while (reader.Read())
                {
                    data.Add(new string[4]);
                    data[data.Count - 1][0] = reader[0].ToString();
                    data[data.Count - 1][1] = reader[1].ToString();
                    data[data.Count - 1][2] = reader[2].ToString();
                    data[data.Count - 1][3] = reader[3].ToString();
                }

                reader.Close();
                sqlConnection.Close();

                return data;
            }
        }

        public List<string[]> ShowMeBalanceProduct()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand("Select  [Products].name , price , state, [Products].id  from [Products] RIGHT JOIN [Users] ON [Users].id = user_id  where user_id = @id  ", sqlConnection);
                sqlCommand.Parameters.AddWithValue("id", DataUser.Id);
                sqlCommand.ExecuteNonQuery();
                SqlDataReader reader = sqlCommand.ExecuteReader();

                List<string[]> data = new List<string[]>();

                while (reader.Read())
                {
                    data.Add(new string[4]);
                    data[data.Count - 1][0] = reader[0].ToString();
                    data[data.Count - 1][1] = reader[1].ToString();
                    data[data.Count - 1][2] = reader[2].ToString();
                    data[data.Count - 1][3] = reader[3].ToString();
                }

                reader.Close();
                sqlConnection.Close();

                return data;
            }
        }


        public int PriceBalance()
        {
            int sum_price = 0;
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand("SELECT price FROM [Products]", sqlConnection);
                sqlCommand.ExecuteNonQuery();

                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int Index_price = reader.GetOrdinal("price");

                        DataProduct.Price = reader.GetInt32(Index_price);

                        sum_price = sum_price + DataProduct.Price;
                    }
                }

                reader.Close();
                sqlConnection.Close();
                return sum_price;
            }
        }





        public void DeleteProduct(int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand("DELETE FROM [Products] WHERE Id = @id ", sqlConnection);
                sqlCommand.Parameters.AddWithValue("id", id);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }

        }

        public void СhangeProduct(Product product)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand("UPDATE [Products] SET name = @name, price = @price, state = @state where id = @id", sqlConnection);
                sqlCommand.Parameters.AddWithValue("name", product.Name);
                sqlCommand.Parameters.AddWithValue("price", product.Price);
                sqlCommand.Parameters.AddWithValue("state", product.State);
                sqlCommand.Parameters.AddWithValue("id", product.Id);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }

        }

    }
}
