﻿using Models.UserModel;
using System.Collections.Generic;

namespace Interfaces.IUserRepository
{
 
    public interface IUserRepository
    {
        bool RegisterUser(User user);

        bool LoginUser(User user);

        List<string[]> UserList();

        void EditUser(User user);

        void DeleteUser(int id);

        bool LoginAdmin(User user);
    }
}
