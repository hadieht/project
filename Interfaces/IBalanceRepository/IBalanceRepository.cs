﻿using System.Collections.Generic;

namespace Interfaces.IBalanceRepository
{
    public interface IBalanceRepository
    {
        void СhangeBalanceMain();

        void СhangeBalanceAccumulation();

        void СhangeBalanceStash();

        int SumBalance();

        void LoginBalance();

        void RegBalance();

        void RemoveBalanceMain();

        void RemoveBalanceAccumulation();

        void RemoveBalanceStash();

        List<string[]> ShowBalanceUsers();

    }
}
