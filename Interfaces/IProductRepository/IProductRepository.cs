﻿using Models.ProductModel;
using System.Collections.Generic;

namespace Interfaces.IProductRepository
{
  
    public interface IProductRepository
    {
        void AddProduct(Product product);

        void DeleteProduct(int id);

        void СhangeProduct(Product product);

        int PriceBalance();

        List<string[]> ShowBalanceProduct();

        List<string[]> ShowMeBalanceProduct();

    }
}
