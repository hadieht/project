﻿using System;
using System.Windows.Forms;
using Repositories.UserRepository;
using Models.UserModel;
using Repositories.BalanceRepository;

namespace Forms
{
    public partial class SignIn : Form
    {

        public SignIn()
        {
            InitializeComponent();  
        }

        private void Singn_Click(object sender, EventArgs e)
        {
            
            User user = new User();
            user.Login = LoginText.Text;
            user.Password = PasswordText.Text;
            UserRepository userRepository = new UserRepository();
            if (userRepository.LoginUser(user))
            {
                BalanceRepository balanceRepository = new BalanceRepository();
                balanceRepository.LoginBalance();
                Hide();
                MainUser mainUser = new MainUser();
                mainUser.Show();
                this.Close();
            }
            else
            {
                CleanForm();
                MessageBox.Show("Неверный логин или пароль");
            }
        }

        private void Back_Click(object sender, EventArgs e)
        {
           
            Hide();
            MainFamily main = new MainFamily();
            main.Show();
        }

        private void CleanForm()
        {
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = String.Empty;
                }
            }
        }

       
    }
}
