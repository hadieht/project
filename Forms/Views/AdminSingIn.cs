﻿using System;
using Forms.Views;
using Models.UserModel;
using System.Windows.Forms;
using Repositories.UserRepository;

namespace Forms.Views
{
    public partial class AdminSingIn : Form
    {
        public AdminSingIn()
        {
            InitializeComponent();
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            MainFamily mainFamily = new MainFamily();
            Hide();
            mainFamily.Show();
            Close();
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            User user = new User();
            user.Login = LoginText.Text;
            user.Password = PasswordText.Text;
            UserRepository userRepository = new UserRepository();
            if (userRepository.LoginAdmin(user))
            {
                
                Hide();
                Admin admin = new Admin();
                admin.Show();
                this.Close();
            }
            else
            {
                
                MessageBox.Show("Неверный логин или пароль");
            }
        }
    }
}
