﻿using System;
using System.Windows.Forms;
using Repositories.ProductRepository;
using Models.ProductModel;

namespace Forms
{
    public partial class EditExpence : Form
    {
         private int _selectRow;
        private string _Id;

        public EditExpence()
        {
            InitializeComponent();

            ProductRepository productRepository = new ProductRepository();

            foreach (string[] s in productRepository.ShowMeBalanceProduct())
                DataSource.Rows.Add(s);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Expence expence = new Expence();
            Hide();
            expence.Show();
            Close();
            
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ProductText.Text) && !string.IsNullOrEmpty(PriceText.Text) && !string.IsNullOrEmpty(StateText.Text))
            {
                try
                {
                    Product product = new Product();
                    product.Name = ProductText.Text;
                    product.Price = Int32.Parse(PriceText.Text);
                    product.State = StateText.Text;
                    product.Id = Int32.Parse(_Id);

                    ProductRepository productRepository = new ProductRepository();
                    productRepository.СhangeProduct(product);

                    CleanForm();
                    MessageBox.Show("Продукт обновлён", "Edit");
                }
                catch(Exception)
                {
                    MessageBox.Show("Ошибка! Вы не можете добавить в цену данное значение, пожалуста перепроверьте ваше значение! Вы можете добавить только число", "Error");
                }
            }
            else
            {
                MessageBox.Show("Вы не можете оставить пустые поля", "Error");
            }


        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            ProductRepository productRepository = new ProductRepository();
            productRepository.DeleteProduct(Int32.Parse(_Id));
            MessageBox.Show("Продукт удалён","Delete");
        }

       

        private void DataSource_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            _selectRow = DataSource.CurrentCell.RowIndex;
            DataGridViewRow row = DataSource.Rows[_selectRow];
            ProductText.Text = row.Cells[0].Value.ToString();
            StateText.Text = row.Cells[2].Value.ToString();
            PriceText.Text = row.Cells[1].Value.ToString();
            _Id = row.Cells[3].Value.ToString();

        }



        private void CleanForm()
        {
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = String.Empty;
                }
            }
        }

       
    }
}
