﻿using Forms.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Forms
{
    public partial class MainFamily : Form
    {
        public MainFamily()
        {
            InitializeComponent();
        }

        private void SingIn_Click(object sender, EventArgs e)
        {
            Hide();
            SignIn signin = new SignIn();
            signin.Show();
            

        }

        private void SingUp_Click(object sender, EventArgs e)
        {
            Hide();
            SingUp singup = new SingUp();
            singup.Show();
            
        }

  


        private void Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Admin_Click(object sender, EventArgs e)
        {
            AdminSingIn adminSingIn = new AdminSingIn();
            Hide();
            adminSingIn.Show();
            
        }
    }
}
