﻿using System;
using System.Windows.Forms;
using Repositories.UserRepository;
using Models.UserModel;
using Forms.Views;

namespace Forms.Views
{
    public partial class Admin : Form
    {

        private int _selectRow;
        private string _Id;

        public Admin()
        {
            InitializeComponent();


            UserRepository userRepository = new UserRepository();

            foreach (string[] s in userRepository.UserList())
                DataSource.Rows.Add(s);


        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(NameText.Text) && !string.IsNullOrEmpty(LastNameText.Text) && !string.IsNullOrEmpty(LoginText.Text) && !string.IsNullOrEmpty(PasswordText.Text))
            {
                
                    User user = new User();
                    user.Id = Int32.Parse(_Id);
                    user.Name = NameText.Text;
                    user.LastName = LastNameText.Text;
                    user.Login = LoginText.Text;
                    user.Password = PasswordText.Text;

                    UserRepository userRepository = new UserRepository();
                    userRepository.EditUser(user);
                    MessageBox.Show("Вы успешно изменили", "Edit");
                    


            }
            else
            {
                CleanForm();
                MessageBox.Show("Вы не можете оставить пустые поля", "Error");
            }

        }

        private void DataSource_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            _selectRow = DataSource.CurrentCell.RowIndex;
            DataGridViewRow row = DataSource.Rows[_selectRow];
            _Id = row.Cells[0].Value.ToString();
            NameText.Text = row.Cells[1].Value.ToString();
            LastNameText.Text = row.Cells[2].Value.ToString();
            LoginText.Text = row.Cells[4].Value.ToString();
           PasswordText.Text= row.Cells[5].Value.ToString();
            

        }

        private void Exit_Click(object sender, EventArgs e)
        {
            MainFamily mainFamily = new MainFamily();
            Hide();
            mainFamily.Show();
            Close();
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            UserRepository userRepository = new UserRepository();
            userRepository.DeleteUser(Int32.Parse(_Id));

            CleanForm();
            MessageBox.Show("Вы успешно удалил", "Delete");
        }


        private void CleanForm()
        {
            foreach (var c in this.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)c).Text = String.Empty;
                }
            }
        }

    }
}
