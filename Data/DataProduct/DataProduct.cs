﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.DataProduct
{
    public static class DataProduct
    {
        public static int Id { get; set; }

        public static string Name { get; set; }

        public static int Price { get; set; }

        public static string State { get; set; }

        public static int IdUser { get; set; }
    }
}
